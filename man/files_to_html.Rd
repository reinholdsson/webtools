\name{files_to_html}
\alias{files_to_html}
\title{Convert files to HTML string}
\usage{
  files_to_html(files)
}
\arguments{
  \item{files}{vector of file paths}
}
\description{
  Reads Cascading Style Sheet (CSS) and JavaScript (JS)
  files and then converts them to a HTML character string.
}

