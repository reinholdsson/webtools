#' Load packages
#' 
#' Install and/or load packages
#' 
#' @param pkgs character vector of package names to install
#' @param repo cran repository (default = "http://ftp.sunet.se/pub/lang/CRAN/")
#' @export
#' @examples
#' load_packages(c("knitr", "markdown"))
load_packages <- function(pkgs, repo = "http://ftp.sunet.se/pub/lang/CRAN/"){
  invisible(lapply(pkgs, function(pkg) {
    req <- function(x) require(x, character.only = TRUE)
    if(!suppressWarnings(req(pkg))){
      install.packages(pkg, repos = repo)
      req(pkg)
    }     
  }))
}