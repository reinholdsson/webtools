#' Convert files to HTML string
#' 
#' Reads Cascading Style Sheet (CSS) and JavaScript (JS) files
#' and then converts them to a HTML character string.
#' @export
#' @param files vector of file paths
files_to_html <- function(files) {
  html <- lapply(files, function(file) {
    extension <- tolower(file_ext(file))
    if (extension == 'css') {
      if (substr(file, 1,5) == "http:") {
        html <- sprintf('<link type="text/css" href="%s" rel="stylesheet">', file)
      } else {
        html <- sprintf('<style type="text/css">%s</style>', readChar(file, file.info(file)$size))
      }
      
    } else if (extension == 'js') {
      if(substr(file, 1,5) == "http:") {
        html <- sprintf('<script type="text/javascript" src="%s"></script>', file)
      } else {
        html <- sprintf('<script language="javascript">%s</script>', readChar(file, file.info(file)$size))
      }
      
    } else if (extension == 'html') {
      if (substr(file, 1,5) == "http:") {
        # TODO
      } else {
        html <- readChar(file, file.info(file)$size)
      }
      
    } else { stop(extension, " is not supported") }
    return(html)
  })
  html <- paste(html, collapse = "")
  return(html)
}