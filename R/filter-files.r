#' Filter file paths
#' 
#' Filter a character vector of file paths, only returning the ones with the specified file extension.
#' @export
#' @param files character vector of file paths
#' @param extensions character vector of allowed file extensions; e.g. c("css", "js")
filter_files <- function(files, extensions) {
  files <- files[tolower(file_ext(files)) %in% tolower(extensions)]
  return(files)
}
