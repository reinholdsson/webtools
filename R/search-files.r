#' Search for files and return their paths
#' 
#' Search for files in a specific folder. 
#' The function returns the search results as character vector containing the file paths.
#' @export
#' @param path search path
#' @param extensions character vector of allowed file extensions
#' @param recursive whether to search path recursively (default = TRUE)
search_files <- function(path, extensions = NULL, recursive = TRUE) {
  files <- list.files(path, recursive = recursive, full.names = TRUE)
  if (!is.null(extensions)) {
    files <- filter_files(files, extensions)
  }
  return(files)
}
