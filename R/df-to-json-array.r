#' Data frame to JSON array
#'
#' Converts a data frame to a JSON array
#' http://theweiluo.wordpress.com/2011/09/30/r-to-json-for-d3-js-and-protovis/
#' 
#' @param df data frame
#' 
#' @export 
#'
df_to_json_array <- function(df){
  col_names <- colnames(df)
  
  name.value <- function(i){
    quote <- '';
    if (class(df[, i]) != 'numeric'){
      quote <- '"';
    }
    paste('"', i, '" : ', quote, df[, i], quote, sep= '')
  }
  
  objs <- apply(sapply(col_names, name.value), 1, function(x){ paste(x, collapse = ', ') })
  objs <- paste('{', objs, '}')
  
  res <- paste('[', paste(objs, collapse = ', '), ']')
  return(res)
}
