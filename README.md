webtools
===============================

## INSTALL

Use `devtools` for easy installation

    library(devtools)
    install_github('webtools', 'reinholdsson')
    library(webtools)
